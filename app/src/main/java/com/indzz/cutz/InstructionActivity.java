package com.indzz.cutz;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.indzz.cutz.helper.ActivityIntentUtils;
import com.indzz.cutz.helper.AnalyticsHelper;
//import com.indzz.cutz.helper.Member;
import com.indzz.cutz.helper.Member;
import com.indzz.cutz.helper.PreferenceHelper;
import com.google.firebase.analytics.FirebaseAnalytics;


public class InstructionActivity extends AppCompatActivity {


    private ViewPager mViewPager;
    private InstructionSlideAdapter mPagerAdapter;

    private Button skipButton;
    private Button loginButton;
    private Button loginAsProButton;
    private Button registerButton;

    private final int REQUEST_LOGIN = 11;
    private final int REQUEST_REGISTER = 12;
    private final int REQUEST_LOGIN_AS_PRO = 13;

    protected FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instruction);

        // Obtain the FirebaseAnalytics instance.
        /*
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        AnalyticsHelper.setup(mFirebaseAnalytics);

        // Log event
        AnalyticsHelper.logEvent(mFirebaseAnalytics, FirebaseAnalytics.Event.TUTORIAL_BEGIN, "Intro", "Read introduction page");
*/

        // Tutorial Slider
        mViewPager = (ViewPager) findViewById(R.id.view_pager);
        mPagerAdapter = new InstructionSlideAdapter(getSupportFragmentManager());
        mViewPager.setAdapter(mPagerAdapter);

        // Button Setup
        skipButton = (Button) findViewById(R.id.skip_button);
        loginButton = (Button) findViewById(R.id.login_button);
        loginAsProButton = (Button) findViewById(R.id.login_as_pro);
        registerButton = (Button) findViewById(R.id.register_button);
        setupButtonsListener();

        // Page Indicator
        setupPageIndicator();

        // Show login/register button only if non-member
        if (Member.getInstance(this).isMember()) {
            findViewById(R.id.buttons_container).setVisibility(View.GONE);
        }

        // Hide skip button if requested
        if (getIntent().getBooleanExtra("hide_skip", false)) {
            skipButton.setVisibility(View.GONE);
        }


        // Back button
        if (getIntent().getBooleanExtra("show_back", false)) {
            ImageButton backButton = (ImageButton) findViewById(R.id.back_button);
            backButton.setVisibility(View.VISIBLE);
            backButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    onBackPressed();
                }
            });
        }

        // Listen to account state change broadcaster
        LocalBroadcastManager.getInstance(this).registerReceiver(mMemberStateChangedReceiver, new IntentFilter(Member.MEMBER_STATE_CHANGE_KEY));
    }

    @Override
    protected void onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMemberStateChangedReceiver);
        super.onDestroy();
    }

    private BroadcastReceiver mMemberStateChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.d("receiver", "InstructionActivity - mMemberStateChangedReceiver");
            if (Member.getInstance(getApplicationContext()).isMember()) {
                skipInstructionScreens(intent.getExtras());
            }
        }
    };

    protected void skipInstructionScreens(Bundle data) {
        // Remember to skip the instruction
        PreferenceHelper.getInstance(getBaseContext()).update("instruction_done", true);

        // Go to home activity
        //Intent intent = ActivityIntentUtils.professionalHomeIntent(InstructionActivity.this);
        Intent intent = ActivityIntentUtils.homeIntent(InstructionActivity.this);

        if (data != null && data.containsKey("next")) {
            intent.putExtra("next", data.getString("next"));
        }

        startActivity(intent);
        finish();
    }

    protected void setupButtonsListener() {
        // Skip the instruction screen
        skipButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                skipInstructionScreens(null);
            }
        });

        // Login button
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Go to login screen
                startActivityForResult(ActivityIntentUtils.loginIntent(InstructionActivity.this), REQUEST_LOGIN);
            }
        });

        // Register button
        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Go to register screen
                startActivityForResult(ActivityIntentUtils.registerIntent(InstructionActivity.this), REQUEST_REGISTER);
            }
        });

        loginAsProButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Go to login as pro
                startActivityForResult(ActivityIntentUtils.loginAsProIntent(InstructionActivity.this), REQUEST_LOGIN_AS_PRO);
            }
        });
    }

    protected void setupPageIndicator() {
        // Indicator
        final RadioGroup indicatorGroup = (RadioGroup) findViewById(R.id.indicator_group);
        for (int i = 0; i < mPagerAdapter.getCount(); i++) {
            RadioButton bullet = new RadioButton(this);
            bullet.setEnabled(false);
            bullet.setPadding(10, 0, 10, 0);
            bullet.setButtonDrawable(ContextCompat.getDrawable(this, R.drawable.tutorial_bullet));
            indicatorGroup.addView(bullet);

            Log.i("indzz", "i = " + i);
        }
        ((RadioButton) indicatorGroup.getChildAt(0)).setChecked(true);

        mViewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                ((RadioButton) indicatorGroup.getChildAt(position)).setChecked(true);
            }
        });
    }


}
