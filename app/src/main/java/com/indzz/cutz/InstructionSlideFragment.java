package com.indzz.cutz;


import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class InstructionSlideFragment extends Fragment {

    private
    @DrawableRes
    int background;
    private
    @DrawableRes
    int icon;
    private
    @StringRes
    int title;
    private
    @StringRes
    int description;

    private static final String TAG = "MyActivity";


    public static InstructionSlideFragment newInstance(@DrawableRes int background, @DrawableRes int icon, @StringRes int title, @StringRes int description) {
        InstructionSlideFragment fragment = new InstructionSlideFragment();

        Bundle args = new Bundle();
        args.putInt("background", background);
        args.putInt("icon", icon);
        args.putInt("title", title);
        args.putInt("description", description);

        fragment.setArguments(args);
        return fragment;
    }


    public InstructionSlideFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            background = getArguments().getInt("background");
            icon = getArguments().getInt("icon");
            title = getArguments().getInt("title");
            description = getArguments().getInt("description");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_instruction_slide, container, false);

        // Background
        ((ImageView) view.findViewById(R.id.bgPhoto)).setImageResource(background);

        // Icon
        //((ImageView)view.findViewById(R.id.icon)).setBackgroundResource(icon);

        // Heading
        //((TextView)view.findViewById(R.id.heading)).setText(title);

        // Description



        Log.d(TAG, description+"");

        //((TextView) view.findViewById(R.id.instruction_description)).setText(description);

        return view;
    }

}
