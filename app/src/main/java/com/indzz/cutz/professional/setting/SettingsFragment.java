package com.indzz.cutz.professional.setting;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.facebook.login.LoginManager;
import com.indzz.cutz.InstructionActivity;
import com.indzz.cutz.R;
import com.indzz.cutz.helper.ActivityIntentUtils;
import com.indzz.cutz.helper.Member;
import com.indzz.cutz.helper.SimpleDividerItemDecoration;

import java.util.ArrayList;
import java.util.List;

public class SettingsFragment extends Fragment {
    private SettingsRecyclerViewAdapter mAdapter;
    private List<SettingItem> mDataset;

    private Member member;

    public SettingsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize Facebook SDK
        //FacebookSdk.sdkInitialize(getActivity().getApplicationContext());

        Log.i("indzz-setting", " ==created== ");

        member = Member.getInstance(getActivity());
        setupDataList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.i("indzz-setting", " == created view == ");



        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        // Set the adapter
        if (view instanceof RecyclerView) {
            Context context = view.getContext();
            RecyclerView recyclerView = (RecyclerView) view;
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
            recyclerView.addItemDecoration(new SimpleDividerItemDecoration(getActivity(), true));

            mAdapter = new SettingsRecyclerViewAdapter(getActivity(), getItems(), new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    SettingItem item = mDataset.get((Integer)v.getTag());

                    Intent intent;
                    switch (item.getId()) {
                        case SettingItem.ITEM_ID_ABOUT_CUTZ:
                            startActivity(ActivityIntentUtils.proAboutUsIntent(getActivity()));
                            break;

                        case SettingItem.ITEM_ID_CONTACT_US:
                            startActivity(ActivityIntentUtils.proContactUsIntent(getActivity()));
                            break;

                        case SettingItem.ITEM_ID_TCS:
                            startActivity(ActivityIntentUtils.proTermsIntent(getActivity()));
                            break;

                        case SettingItem.ITEM_ID_FUNCTION_INTRODUCTION:
                            intent = new Intent(getActivity(), InstructionActivity.class);
                            intent.putExtra("hide_skip", true);
                            intent.putExtra("show_back", true);
                            startActivity(intent);
                            break;
                        case SettingItem.ITEM_ID_LOGOUT:
                            logout();
                            break;
                    }
                }
            });
            recyclerView.setAdapter(mAdapter);
        }

        return view;
    }



    protected void setupDataList() {
        List<SettingItem> items = new ArrayList<>();
        items.add(new SettingItem(SettingItem.ITEM_ID_EMPTY));
        //items.add(new SettingItem(SettingItem.ITEM_ID_ABOUT_EDR, getString(R.string.about_edr)));
        items.add(new SettingItem(SettingItem.ITEM_ID_LANGUAGE, getString(R.string.language)));

        items.add(new SettingItem(SettingItem.ITEM_ID_EMPTY));

        items.add(new SettingItem(SettingItem.ITEM_ID_ABOUT_CUTZ, getString(R.string.about_cutz)));
        items.add(new SettingItem(SettingItem.ITEM_ID_CONTACT_US, getString(R.string.contact_us)));

        items.add(new SettingItem(SettingItem.ITEM_ID_TCS, getString(R.string.terms_and_cons)));
        items.add(new SettingItem(SettingItem.ITEM_ID_VERSION, getString(R.string.version)));

        if (member.isMember()) {
            items.add(new SettingItem(SettingItem.ITEM_ID_EMPTY));
            items.add(new SettingItem(SettingItem.ITEM_ID_LOGOUT, getString(R.string.logout)));
        }

        mDataset = items;
    }

    protected List<SettingItem> getItems() {
        return mDataset;
    }

    protected void logout() {
        Member.getInstance(getActivity().getApplicationContext()).forgetLoginCredentials();

        LocalBroadcastManager.getInstance(getActivity()).sendBroadcast(new Intent(Member.MEMBER_STATE_CHANGE_KEY));
        getActivity().finish();

        // Logout Facebook
        LoginManager.getInstance().logOut();
    }
}
