package com.indzz.cutz.professional.setting;

/**
 * Created by indzz on 27/9/2016.
 */

public class SettingItem {
    private int id;
    private String label;

    public static final int ITEM_ID_EMPTY = 0;


    public static final int ITEM_ID_ACCOUNT_SETUP = 1;
    public static final int ITEM_ID_ABOUT_CUTZ = 2;
    public static final int ITEM_ID_FUNCTION_INTRODUCTION = 3;

    public static final int ITEM_ID_TCS = 4;

    public static final int ITEM_ID_LOGOUT = 6;
    public static final int ITEM_ID_LANGUAGE = 7;
    public static final int ITEM_ID_SHOP_LIST = 8;
    public static final int ITEM_ID_CONTACT_US = 9;
    public static final int ITEM_ID_VERSION = 10;


    public SettingItem(int id) {
        this.id = id;
    }

    public SettingItem(int id, String label) {
        this.id = id;
        this.label = label;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
}
