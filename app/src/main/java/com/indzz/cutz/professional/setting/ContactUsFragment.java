package com.indzz.cutz.professional.setting;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.indzz.cutz.R;
import com.indzz.cutz.helper.Member;

import java.util.ArrayList;
import java.util.List;

public class ContactUsFragment extends Fragment {
    private SettingsRecyclerViewAdapter mAdapter;
    private List<SettingItem> mDataset;

    private Member member;

    public ContactUsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize Facebook SDK
        //FacebookSdk.sdkInitialize(getActivity().getApplicationContext());

        Log.i("indzz-setting", " ==created== ");

        member = Member.getInstance(getActivity());
        //setupDataList();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.i("indzz-setting", " == created view == ");


        // Inflate the layout for this fragment


        View view = inflater.inflate(R.layout.fragment_settings_contact, container, false);


        return view;
    }


    protected void setupDataList() {
        List<SettingItem> items = new ArrayList<>();
        items.add(new SettingItem(SettingItem.ITEM_ID_EMPTY));
        //items.add(new SettingItem(SettingItem.ITEM_ID_ABOUT_EDR, getString(R.string.about_edr)));
        items.add(new SettingItem(SettingItem.ITEM_ID_LANGUAGE, getString(R.string.language)));
        items.add(new SettingItem(SettingItem.ITEM_ID_SHOP_LIST, getString(R.string.shop_list)));

        mDataset = items;
    }

    protected List<SettingItem> getItems() {
        return mDataset;
    }

}
