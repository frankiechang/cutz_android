package com.indzz.cutz.professional.setting;

/**
 * Created by indzz on 27/9/2016.
 */

public class ProActivities {
    public static class HomeActivity extends NavDrawerActivity {}
    public static class SettingsActivity extends NavDrawerActivity {}
    public static class UsefulInfoActivity extends NavDrawerActivity {}

    // Find doctor
    public static class SpecialityListActivity extends NavDrawerActivity {}
    public static class DistrictListActivity extends NavDrawerActivity {}
    public static class NearbyListActivity extends NavDrawerActivity {}
    public static class AdvancedSearchActivity extends NavDrawerActivity {}

    // Account
    public static class FavouriteListActivity extends NavDrawerActivity {}
    public static class BookingRecordActivity extends NavDrawerActivity {}
//    public static class EcardPortalActivity extends NavDrawerActivity {}
}
