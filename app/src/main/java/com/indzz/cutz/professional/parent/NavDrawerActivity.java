package com.indzz.cutz.professional.setting;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indzz.cutz.R;
import com.indzz.cutz.helper.ActivityIntentUtils;
import com.indzz.cutz.helper.Member;

import de.hdodenhof.circleimageview.CircleImageView;

//import com.indzz.cutz.com.indzz.cutz.entities.FamilyMember;
//import com.indzz.cutz.home.HomeFragment;
//import com.indzz.cutz.search.advanced.AdvancedSearchFragment;
//import com.indzz.cutz.search.result.HairSalonActivity;

/**
 * Activity skeleton with navigation drawer
 * Main content could be present in a fragment
 * This design pattern provides a consistent way to handle the navigation drawer
 * <p>
 * Inspired by Android N's Settings app
 * Ref: https://android.googlesource.com/platform/packages/apps/Settings.git/
 */
public class NavDrawerActivity extends AppCompatActivity
        //implements NavigationView.OnNavigationItemSelectedListener, AdvancedSearchFragment.OnAdvancedSearchFragmentInteractionListener {
        implements NavigationView.OnNavigationItemSelectedListener {

    public static final String EXTRA_ID = "main.id";
    public static final String EXTRA_FRAGMENT_NAME = "main.fragment_name";
    public static final String EXTRA_SHOW_LOGO = "main.show_logo";
    public static final String EXTRA_ARGUMENTS = "main.arguments";
    public static final String EXTRA_TITLE = "main.title";
    public static final String EXTRA_WITH_TRANSITION = "main.transition";

    private TextView nameView;
    private TextView emailView;
    private CircleImageView avatarView;
    private View guestButtonContainer;

    private Member member;
    //private FamilyMember myProfile;
    private boolean profileHasUpdate = false;

    protected FirebaseAnalytics mFirebaseAnalytics;

    protected int pageId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_professional);

        // Obtain the FirebaseAnalytics instance.
//        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
//        AnalyticsHelper.setup(mFirebaseAnalytics);

        Intent intent = getIntent();

        pageId = intent.getIntExtra(EXTRA_ID, R.id.nav_home);

        member = Member.getInstance(getApplicationContext());
        //myProfile = member.getProfile();

        // Toolbar setup
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Show or hide nav bar logo
        showOrHideLogo(toolbar, intent.getBooleanExtra(EXTRA_SHOW_LOGO, pageId == R.id.nav_home));

        // Navigation Drawer
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        setupNavigationView();

        // Handle extras
        String fragmentName = intent.getStringExtra(EXTRA_FRAGMENT_NAME);

        if (fragmentName == null) {
            fragmentName = SettingsFragment.class.getName();
            //throw new RuntimeException("EXTRA_FRAGMENT_NAME must be provided");
        }

        Bundle args = intent.getBundleExtra(EXTRA_ARGUMENTS);
        boolean addToBackStack = false; // Should not add to back stack for initial fragment
        Object title = intent.getExtras() != null ? intent.getExtras().get(EXTRA_TITLE) : null;
        boolean withTransition = intent.getBooleanExtra(EXTRA_WITH_TRANSITION, true);

        // Main content fragment
        switchToFragment(fragmentName, args, addToBackStack, title, withTransition);

        // Listen to account state change broadcaster
        LocalBroadcastManager.getInstance(this).registerReceiver(mMemberStateChangedReceiver, new IntentFilter(Member.MEMBER_STATE_CHANGE_KEY));
//        LocalBroadcastManager.getInstance(this).registerReceiver(mAvatarChangedReceiver, new IntentFilter(FamilyMember.BROADCASTER_UPDATE_AVATAR));
//        LocalBroadcastManager.getInstance(this).registerReceiver(mMemberInfoChangedReceiver, new IntentFilter(FamilyMember.BROADCASTER_UPDATE_PROFILE));
    }

    @Override
    protected void onDestroy() {
        // Unregister since the activity is about to be closed.
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMemberStateChangedReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mAvatarChangedReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mMemberInfoChangedReceiver);
        super.onDestroy();
    }

    @Override
    protected void onResume() {
        super.onResume();

        Log.i("onResume", "onResume | has update = " + (profileHasUpdate ? "Y" : "N"));
        if (profileHasUpdate) {
            profileHasUpdate = false;

//            member.refreshProfile();
//            myProfile = member.getProfile();
            updateAccountViews();
        }
    }

    private BroadcastReceiver mMemberStateChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            /*Member.getInstance(getApplicationContext()).refreshProfile();
            updateAccountViews();*/

            profileHasUpdate = true;
        }
    };

    private BroadcastReceiver mAvatarChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Log.i("i", "mAvatarChangedReceiver");
            /*Member.getInstance(getApplicationContext()).refreshProfile();
            updateAccountViews();*/

            profileHasUpdate = true;
        }
    };

    private BroadcastReceiver mMemberInfoChangedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            /*Member.getInstance(getApplicationContext()).refreshProfile();
            updateAccountViews();*/

            profileHasUpdate = true;
        }
    };

    protected void showOrHideLogo(Toolbar toolbar, boolean isShow) {
        if (isShow) {
            // Hide title text
            getSupportActionBar().setDisplayShowTitleEnabled(false);

            // Show logo
            //toolbar.findViewById(R.id.nav_logo).setVisibility(View.VISIBLE);
        } else {
            // Show title text
            getSupportActionBar().setDisplayShowTitleEnabled(true);

            // Hide logo
            //toolbar.findViewById(R.id.nav_logo).setVisibility(View.GONE);
        }
    }

    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    protected void setupNavigationView() {
        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        //navigationView.setCheckedItem(pageId);

        View headerView = navigationView.getHeaderView(0);

        nameView = (TextView) headerView.findViewById(R.id.nameView);
        emailView = (TextView) headerView.findViewById(R.id.emailView);
        //avatarView = (CircleImageView) headerView.findViewById(R.id.avatar);
        guestButtonContainer = headerView.findViewById(R.id.guest_buttons);

        Button loginButton = (Button) headerView.findViewById(R.id.login_button);
        Button registerButton = (Button) headerView.findViewById(R.id.register_button);

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(ActivityIntentUtils.loginIntent(NavDrawerActivity.this));
            }
        });

        registerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(ActivityIntentUtils.registerIntent(NavDrawerActivity.this));
            }
        });

        updateAccountViews();
    }

    protected void updateAccountViews() {
        /*
        boolean isMember = member.isMember();
        if (isMember) {
            // Member => set text and show relevant views
            emailView.setText(member.getEmail());
            emailView.setVisibility(View.VISIBLE);


            if (myProfile != null && !TextUtils.isEmpty(myProfile.getName())) {
                nameView.setText(myProfile.getName());
                nameView.setVisibility(View.VISIBLE);
            } else {
                nameView.setVisibility(View.GONE);
            }

//            Log.i("onResume", "myProfile != null | " + (myProfile != null ? "Y" : "N"));
//            Log.i("onResume", "uri = " + myProfile.getAvatarUri());
//            Log.i("onResume", "uriString = " + myProfile.getAvatarUriString());
            if (myProfile != null && myProfile.getAvatarUri() != null) {
//                Log.i("onResume", "getAvatarUri() = " + myProfile.getAvatarUri());
                try {
                    avatarView.setImageBitmap(ImageUtils.decodeSampledBitmapFromResource(this, myProfile.getAvatarUri(), 70, 70));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                avatarView.setImageResource(R.drawable.avatar_default);
            }

            // Hide login & register buttons
            guestButtonContainer.setVisibility(View.GONE);
        } else {
            // Guest
            // Set name
            nameView.setText(getString(R.string.non_member));
            nameView.setVisibility(View.VISIBLE);

            // Hide email
            emailView.setVisibility(View.GONE);

            // Show login & register buttons
            guestButtonContainer.setVisibility(View.VISIBLE);

            // Change to default avatar
            avatarView.setImageResource(R.drawable.avatar_default);
        }
        */
    }

    protected Member getMember() {
        return Member.getInstance(getApplicationContext());
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        // Start the activity only if current screen is not the target activity
        if (pageId != id) {


            if (id == R.id.nav_home) {
                Intent intent = ActivityIntentUtils.homeIntent(this);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                startActivity(intent);
            }else if(id == R.id.nav_setting){
                //Log.i("indzz-nav"," === Check Setting ===");
                startActivity(ActivityIntentUtils.proSettingsIntent(this));
            }



            /*else if (id == R.id.nav_speciality_dr) {
                startActivity(ActivityIntentUtils.specialityListIntent(this));
            }  else if (id == R.id.nav_advanced_search) {
                startActivity(ActivityIntentUtils.advancedSearchIntent(this));
            } else if (id == R.id.nav_district_dr) {
                startActivity(ActivityIntentUtils.districtListIntent(this));
            } else if (id == R.id.nav_nearby_dr) {
                HairSalonActivity.Criteria criteria = new HairSalonActivity.Criteria();
                criteria.setNearby(true);

                Intent intent = ActivityIntentUtils.doctorListIntent(this, criteria, getString(R.string.nearby_dr));
                intent.putExtra("filter_speciality", true);

                startActivity(intent);
            } else if (id == R.id.nav_useful_info) {
                startActivity(ActivityIntentUtils.usefulInfoIntent(this));
            } else if (id == R.id.nav_settings) {
                startActivity(ActivityIntentUtils.settingsIntent(this));
            } else {
                // Member only pages
                if (!Member.getInstance(getApplicationContext()).isMember()) {
                    // Not member => can't show favourite doctors
                    Member.createLoginPrompt(this).show();
                } else {
                    if (id == R.id.nav_favourite_dr) {
                        startActivity(ActivityIntentUtils.favouriteDoctorListIntent(this));
                    } else if (id == R.id.nav_ecard) {
                        startActivity(ActivityIntentUtils.ecardPortalIntent(this, getMember().isHasEcard()));
                    } else if (id == R.id.nav_consult_record) {
                        startActivity(ActivityIntentUtils.consultationRecordIntent(this));
                    } else if (id == R.id.nav_booking_record) {
                        startActivity(ActivityIntentUtils.appointmentRecordIntent(this));
                    } else if (id == R.id.nav_my_account) {
                        startActivity(ActivityIntentUtils.myAccountIntent(this));
                    }
                }
        }*/

            // must keep the home instance
            if (pageId != R.id.nav_home) {
                finish();
            }

            //Log.i("indzz-nav"," === finish ===");

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private Fragment switchToFragment(String fragmentName, Bundle args, boolean addToBackStack, Object title, boolean withTransition) {
        Fragment f = Fragment.instantiate(this, fragmentName, args);
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.content_main, f);

        if (withTransition) {
            transaction.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        }
        /*if (withTransition) {
            TransitionManager.beginDelayedTransition(mContent);
        }*/
        if (addToBackStack) {
            transaction.addToBackStack("main");
        }
        if (title != null) {
            if (title instanceof String) {
                transaction.setBreadCrumbTitle((String) title);
            } else if (title instanceof Integer) {
                transaction.setBreadCrumbTitle((Integer) title);
            }
        }
        transaction.commitAllowingStateLoss();
        getFragmentManager().executePendingTransactions();
        return f;
    }
}
