package com.indzz.cutz;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by indzz on 26/9/2016.
 */

public class InstructionSlideAdapter extends FragmentStatePagerAdapter {
    private List<InstructionSlideFragment> dataList;

    public InstructionSlideAdapter(FragmentManager fm) {
        super(fm);

        dataList = new ArrayList<>();
        dataList.add(InstructionSlideFragment.newInstance(R.drawable.bg_instruction1, R.drawable.icon_instruction_1, R.string.instruction1_title, R.string.instruction1_description));
        dataList.add(InstructionSlideFragment.newInstance(R.drawable.bg_instruction2, R.drawable.icon_instruction_2, R.string.instruction2_title, R.string.instruction2_description));
        dataList.add(InstructionSlideFragment.newInstance(R.drawable.bg_instruction3, R.drawable.icon_instruction_3, R.string.instruction3_title, R.string.instruction3_description));
    }

    @Override
    public InstructionSlideFragment getItem(int position) {
        return dataList.get(position);
    }

    @Override
    public int getCount() {
        return dataList.size();
    }
}
