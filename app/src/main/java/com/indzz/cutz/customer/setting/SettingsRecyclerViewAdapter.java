package com.indzz.cutz.customer.setting;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.indzz.cutz.R;

import java.util.List;

public class SettingsRecyclerViewAdapter extends RecyclerView.Adapter<SettingsRecyclerViewAdapter.ViewHolder> {

    private final Context mContext;
    private final List<SettingItem> mValues;
    private final View.OnClickListener mListener;

    public SettingsRecyclerViewAdapter(Context context, List<SettingItem> items, View.OnClickListener listener) {
        mContext = context;
        mValues = items;
        mListener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(viewType == 0 ? R.layout.row_setting_empty : R.layout.row_setting, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public int getItemViewType(int position) {
        SettingItem item = mValues.get(position);
        return item.getId() == SettingItem.ITEM_ID_EMPTY ? 0 : 1;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mView.setTag(position);
        holder.mItem = mValues.get(position);

        if (holder.mItem.getId() != SettingItem.ITEM_ID_EMPTY) {
            holder.mContentView.setText(mValues.get(position).getLabel());
            if (holder.mItem.getId() == SettingItem.ITEM_ID_LOGOUT) {
                holder.mContentView.setTextColor(mContext.getResources().getColor(R.color.dark_orange));
            } else {
                holder.mContentView.setTextColor(mContext.getResources().getColor(R.color.black));
            }
        }

        holder.mView.setOnClickListener(mListener);
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public final View mView;
        public final TextView mContentView;
        public SettingItem mItem;

        public ViewHolder(View view) {
            super(view);
            mView = view;
            mContentView = (TextView) view.findViewById(R.id.content);
        }
    }
}
