package com.indzz.cutz.customer.member;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.indzz.cutz.R;
import com.indzz.cutz.helper.Member;

import java.util.HashMap;
import java.util.Map;

public class PersonalInfoFragment extends Fragment {

    private Member member;
    public WebView mWebView;

    public PersonalInfoFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Initialize Facebook SDK
        //FacebookSdk.sdkInitialize(getActivity().getApplicationContext());

        Log.i("indzz-setting", " ==created== ");

        member = Member.getInstance(getActivity());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Log.i("indzz-setting", " == created view == ");


        // Inflate the layout for this fragment
        //View view = inflater.inflate(R.layout.fragment_settings, container, false);


        View view = inflater.inflate(R.layout.fragment_about_us, container, false);

        mWebView = (WebView) view.findViewById(R.id.webview);

        Map<String, String> extraHeaders = new HashMap<String, String>();
        extraHeaders.put("App-Platform", "android/1.0.0");
        extraHeaders.put("App-Language", "");


        mWebView.loadUrl("http://192.168.1.225:8010/api/webview/about", extraHeaders);

        // Enable Javascript
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        // Force links and redirects to open in the WebView instead of in a browser
        mWebView.setWebViewClient(new WebViewClient());

        return view;
    }


}
