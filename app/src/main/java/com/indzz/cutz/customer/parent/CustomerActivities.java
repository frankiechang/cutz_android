package com.indzz.cutz.customer.setting;

/**
 * Created by indzz on 27/9/2016.
 */

public class CustomerActivities {
    public static class HomeActivity extends NavDrawerActivity {}
    public static class SettingsActivity extends NavDrawerActivity {}
    public static class UsefulInfoActivity extends NavDrawerActivity {}

    // Find salon
    public static class FindHairSalonActivity extends NavDrawerActivity {}

    // Account
    public static class FavouriteListActivity extends NavDrawerActivity {}
    public static class BookingRecordActivity extends NavDrawerActivity {}
//    public static class EcardPortalActivity extends NavDrawerActivity {}
}
