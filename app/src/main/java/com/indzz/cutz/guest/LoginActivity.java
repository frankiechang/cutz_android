package com.indzz.cutz.guest;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.login.widget.LoginButton;
import com.indzz.cutz.R;
import com.indzz.cutz.api.DataSyncApi;
import com.indzz.cutz.api.LoginResponse;
import com.indzz.cutz.helper.ActivityIntentUtils;
import com.indzz.cutz.helper.AnalyticsHelper;
import com.indzz.cutz.helper.FormUtils;
import com.indzz.cutz.helper.Member;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A login screen that offers login via email/password.
 */
public class LoginActivity extends AppCompatActivity {
    // UI references.
    private EditText mUsernameView;
    private EditText mPasswordView;
    private LoginButton loginButton;
    private Button display_login_btton;

    private Call<LoginResponse> call;
    private SocialLoginHandler loginHandler;
    private ProgressDialog progressDialog;

    protected FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Obtain the FirebaseAnalytics instance.
/*
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        AnalyticsHelper.setup(mFirebaseAnalytics);
*/
        setContentView(R.layout.activity_login);

        if (Member.getInstance(getApplicationContext()).isMember()) {
            Toast.makeText(this, R.string.already_logged_in, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        loginHandler = new SocialLoginHandler(this);

        // Back button
        ImageButton backButton = (ImageButton) findViewById(R.id.back_button);
        backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // Set up the login form.
        mUsernameView = (EditText) findViewById(R.id.username);
        loginButton = (LoginButton) findViewById(R.id.facebook_button);

        mPasswordView = (EditText) findViewById(R.id.password);
        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mEmailSignInButton = (Button) findViewById(R.id.submit_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });


        // Forgot password button
        findViewById(R.id.forgot_password_button).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, ForgetPasswordActivity.class);
                startActivity(intent);
                finish();
            }
        });


        // simulate click for real fb button
        findViewById(R.id.facebook_login_button).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                loginButton.performClick();
            }
        });


        // go to professional login page
        findViewById(R.id.login_as_pro).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, LoginProfessionalActivity.class);
                startActivity(intent);
                finish();
            }
        });


    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {

        if (call != null) {
            return;
        }


        // Reset errors.
        mUsernameView.setError(null);
        mPasswordView.setError(null);

        // Store values at the time of the login attempt.
        String username = mUsernameView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(password)) {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        } /*else if (!FormUtils.isPasswordValid(password)) {
            mPasswordView.setError(getString(R.string.error_invalid_password));
            focusView = mPasswordView;
            cancel = true;
        }*/

        // Check for a valid email address.
        if (TextUtils.isEmpty(username)) {
            mUsernameView.setError(getString(R.string.error_field_required));
            focusView = mUsernameView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            sendLoginRequest(username, password);
        }
    }

    private void showProgress() {
        progressDialog = ProgressDialog.show(this, getString(R.string.logging_in), "", true);
    }

    private void dismissProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    private void sendLoginRequest(String username, String password) {
        showProgress();

        DataSyncApi api = DataSyncApi.Factory.create(this);
        call = api.login(username, password);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                dismissProgressDialog();

                Log.i("apply", "onResponse - response = " + response);
                Log.i("apply", "onResponse - response.code() = " + response.code());
                Log.i("apply", "onResponse - response.body() = " + response.body());
                Log.i("apply", "onResponse - response.message() = " + response.message());

                LoginResponse data = response.body();

                if (response.code() == 200 && data != null) {

                    // Login Success => remember login credentials
                    Member.getInstance(getApplicationContext()).rememberLoginCredentials(data);

                    Log.i("login-data-indzz", data + "");

                    // Send local broadcast for state changing
                    LocalBroadcastManager.getInstance(LoginActivity.this).sendBroadcast(new Intent(Member.MEMBER_STATE_CHANGE_KEY));

                    // Log event
                    /*AnalyticsHelper.logEvent(mFirebaseAnalytics, FirebaseAnalytics.Event.LOGIN, "Login", "Login");*/

                    // Set result for previous activity
                    setResult(RESULT_OK);
                    finish();
                } else if (response.code() == 403/* && TextUtils.isEmpty(response.errorBody())*/) {
                    // Failed
                    try {
                        // Prompt error message
                        promptError(response.errorBody().string());

                        // Clear value of text fields
                        mUsernameView.setText("");
                        mPasswordView.setText("");
                    } catch (IOException e) {
                        e.printStackTrace();
                        promptError(getString(R.string.failed_to_connect));
                    }
                } else {
                    promptError(getString(R.string.invalid_username_pw));
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                dismissProgressDialog();

                Log.i("error", t.getLocalizedMessage());
                if (LoginActivity.this != null && !call.isCanceled()) {
                    Toast.makeText(LoginActivity.this, R.string.failed_to_connect, Toast.LENGTH_LONG).show();
                    call = null;
                }
            }
        });
    }


    private void promptError(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.error);
        builder.setMessage(message);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mUsernameView.requestFocus();
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (false) {
            // All requests other than Google & Facebook login always have higher priority
        } else {
            loginHandler.onActivityResult(requestCode, resultCode, data);
        }

        Log.i("activityResult", "request = " + requestCode + " | result = " + resultCode);
    }
}

