package com.indzz.cutz.guest;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.indzz.cutz.R;
import com.indzz.cutz.api.DataSyncApi;
import com.indzz.cutz.api.LoginResponse;
import com.indzz.cutz.helper.Member;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A login screen that offers login via email/password.
 */
public class ForgetPasswordActivity extends AppCompatActivity {
    // UI references.
    private EditText mForgetEmailView;
    private EditText mPasswordView;

    private Call<LoginResponse> call;
    private SocialLoginHandler loginHandler;
    private ProgressDialog progressDialog;

    protected FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Obtain the FirebaseAnalytics instance.
/*
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        AnalyticsHelper.setup(mFirebaseAnalytics);
*/
        setContentView(R.layout.activity_forget_password);

        if (Member.getInstance(getApplicationContext()).isMember()) {
            Toast.makeText(this, R.string.already_logged_in, Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        // Back button
        ImageButton backButton = (ImageButton) findViewById(R.id.back_button);
        backButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // Set up the login form.
        mForgetEmailView = (EditText) findViewById(R.id.forget_pw_email);

        Button mEmailSignInButton = (Button) findViewById(R.id.submit_button);
        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

    }

    /**
     * Attempts to sign in or register the account specified by the login form.
     * If there are form errors (invalid email, missing fields, etc.), the
     * errors are presented and no actual login attempt is made.
     */
    private void attemptLogin() {
        /*
        if (call != null) {
            return;
        }
        */

        // Reset errors.
        mForgetEmailView.setError(null);

        // Store values at the time of the login attempt.
        String username_or_email = mForgetEmailView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid email address.
        if (TextUtils.isEmpty(username_or_email)) {
            mForgetEmailView.setError(getString(R.string.error_field_required));
            focusView = mForgetEmailView;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            // Show a progress spinner, and kick off a background task to
            // perform the user login attempt.
            sendForgetRequest(username_or_email);
        }
    }

    private void showProgress() {
        progressDialog = ProgressDialog.show(this, getString(R.string.submitting), "", true);
    }

    private void dismissProgressDialog() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    private void sendForgetRequest(String username_or_email) {
        showProgress();

        DataSyncApi api = DataSyncApi.Factory.create(this);
        call = api.forgetPassword(username_or_email);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                dismissProgressDialog();

                LoginResponse data = response.body();

                if (response.code() == 200 && data != null) {
                    // Login Success => remember login credentials
                    Member.getInstance(getApplicationContext()).rememberLoginCredentials(data);

                    // Send local broadcast for state changing
                    LocalBroadcastManager.getInstance(ForgetPasswordActivity.this).sendBroadcast(new Intent(Member.MEMBER_STATE_CHANGE_KEY));

                    // Log event
                    /*AnalyticsHelper.logEvent(mFirebaseAnalytics, FirebaseAnalytics.Event.LOGIN, "Login", "Login");*/

                    // Set result for previous activity
                    setResult(RESULT_OK);
                    finish();
                } else if (response.code() == 403/* && TextUtils.isEmpty(response.errorBody())*/) {
                    // Failed
                    try {
                        // Prompt error message
                        promptError(response.errorBody().string());

                        // Clear value of text fields
                        mForgetEmailView.setText("");
                    } catch (IOException e) {
                        e.printStackTrace();
                        promptError(getString(R.string.failed_to_connect));
                    }
                } else {
                    promptError(getString(R.string.failed_to_connect));
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                dismissProgressDialog();

                Log.i("error", t.getLocalizedMessage());
                if (ForgetPasswordActivity.this != null && !call.isCanceled()) {
                    Toast.makeText(ForgetPasswordActivity.this, R.string.failed_to_connect, Toast.LENGTH_LONG).show();
                    call = null;
                }
            }
        });
    }



    private void promptError(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.error);
        builder.setMessage(message);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                mForgetEmailView.requestFocus();
            }
        });
        builder.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (false) {
            // All requests other than Google & Facebook login always have higher priority
        } else {
            loginHandler.onActivityResult(requestCode, resultCode, data);
        }

        Log.i("activityResult", "request = " + requestCode + " | result = " + resultCode);
    }
}

