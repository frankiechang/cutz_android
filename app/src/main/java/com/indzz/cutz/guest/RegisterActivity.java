package com.indzz.cutz.guest;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

//import com.indzz.cutz.api.AccountRegisterResponse;
//import com.indzz.cutz.api.DataSyncApi;
import com.indzz.cutz.api.AccountRegisterResponse;
import com.indzz.cutz.api.DataSyncApi;
import com.indzz.cutz.helper.AnalyticsHelper;
import com.indzz.cutz.helper.FormUtils;
import com.indzz.cutz.helper.Member;
import com.google.firebase.analytics.FirebaseAnalytics;

import java.io.IOException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.indzz.cutz.R;

public class RegisterActivity extends AppCompatActivity {

    private EditText emailField;
    private EditText passwordField;
    private EditText repeatPasswordField;
    private EditText nick_nameField;
    private EditText usernameField;
    private EditText telephoneField;
    private Spinner countryField;
    private String salutation = "";
    private RadioGroup radioGroup;
    private RadioButton button_mrs;

    private CheckBox agreeTermsCB;
    private CheckBox subscribePromotionCB;
    //private CheckBox applyEcardCB;

    private Button submitBtn;

    private Call<AccountRegisterResponse> call;
    private SocialLoginHandler loginHandler;

    private ProgressDialog progressDialog;

    protected FirebaseAnalytics mFirebaseAnalytics;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Obtain the FirebaseAnalytics instance.
        /*
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
        AnalyticsHelper.setup(mFirebaseAnalytics);
        */

        setContentView(R.layout.activity_register);
        //loginHandler = new SocialLoginHandler(this);

        // Views
        setupViews();
    }

    protected void setupViews() {
        // Back button
        ImageButton backButton = (ImageButton) findViewById(R.id.back_button);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        // Already a member
        /*findViewById(R.id.login_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
                startActivity(intent);
                finish();
            }
        });*/

        // Assign fields
        emailField = (EditText) findViewById(R.id.email);
        passwordField = (EditText) findViewById(R.id.password);
        repeatPasswordField = (EditText) findViewById(R.id.repeat_password);

        nick_nameField = (EditText) findViewById(R.id.nick_name);
        usernameField = (EditText) findViewById(R.id.username);
        telephoneField = (EditText) findViewById(R.id.telephone);

        countryField = (Spinner) findViewById(R.id.phone_area_spinner);

        button_mrs = (RadioButton) findViewById(R.id.radio_mrs);

        // set up country code spinner
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.phone_area_spinner, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        countryField.setAdapter(adapter);


        radioGroup = (RadioGroup) findViewById(R.id.salutation);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId) {
                    case R.id.radio_mr:
                        salutation = "Mr";
                        break;
                    case R.id.radio_miss:
                        salutation = "Miss";
                        break;
                    case R.id.radio_mrs:
                        salutation = "Mrs";
                        break;
                }
            }
        });


        submitBtn = (Button) findViewById(R.id.submit_button);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validateForm();
            }
        });
    }

    protected void validateForm() {
        boolean hasError = false;

        // Email is required
        String email = emailField.getText().toString();

        if (TextUtils.isEmpty(email)) {
            emailField.setError(getString(R.string.error_email_required));
            hasError = true;
        } else if (!FormUtils.isEmailValid(email)) {
            emailField.setError(getString(R.string.error_invalid_email));
            hasError = true;
        }

        // Check for a valid password, if the user entered one.
        String password = passwordField.getText().toString();
        String repeatPassword = repeatPasswordField.getText().toString();
        if (TextUtils.isEmpty(password)) {
            passwordField.setError(getString(R.string.error_field_required));
            hasError = true;
        } else if (TextUtils.isEmpty(repeatPassword)) {
            repeatPasswordField.setError(getString(R.string.error_password_required));
            hasError = true;
        } else if (!FormUtils.isPasswordValid(password)) {
            passwordField.setError(getString(R.string.error_invalid_password));
            passwordField.setText("");
            repeatPasswordField.setText("");
            hasError = true;
        } else if (!password.equals(repeatPassword)) {
            passwordField.setError(getString(R.string.error_repeat_password_not_match));
            passwordField.setText("");
            repeatPasswordField.setText("");
            hasError = true;
        }

        if (salutation.equals("")) {
            button_mrs.setError(getString(R.string.error_field_required));
            hasError = true;
        }


        String telephone = telephoneField.getText().toString();
        if (TextUtils.isEmpty(telephone)) {
            telephoneField.setError(getString(R.string.error_field_required));
            hasError = true;
        }

        String username = usernameField.getText().toString();
        if (TextUtils.isEmpty(username)) {
            usernameField.setError(getString(R.string.error_field_required));
            hasError = true;
        }

        String nickName = nick_nameField.getText().toString();
        if (TextUtils.isEmpty(nickName)) {
            nick_nameField.setError(getString(R.string.error_field_required));
            hasError = true;
        }


        // Submit only if there is no error
        if (!hasError) {
            submitForm();
        }
    }

    protected void submitForm() {
        progressDialog = ProgressDialog.show(this, getString(R.string.submitting), getString(R.string.registration_form_submitting), true);

        DataSyncApi api = DataSyncApi.Factory.create(this);
        call = api.register(emailField.getText().toString(), usernameField.getText().toString(), passwordField.getText().toString(), nick_nameField.getText().toString(), countryField.getSelectedItem().toString(), telephoneField.getText().toString(), salutation );

        call.enqueue(new Callback<AccountRegisterResponse>() {
            @Override
            public void onResponse(Call<AccountRegisterResponse> call, final Response<AccountRegisterResponse> response) {
                Log.i("apply", "onResponse - response = " + response);
                Log.i("apply", "onResponse - response.code() = " + response.code());
                Log.i("apply", "onResponse - response.body() = " + response.body());
                Log.i("apply", "onResponse - response.message() = " + response.message());

                if (response.code() == 200) {
                    // Log event
//                    AnalyticsHelper.logEvent(mFirebaseAnalytics, FirebaseAnalytics.Event.SIGN_UP, "Registration", "Registration");

                    AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                    builder.setTitle(R.string.member_register_done);
                    builder.setMessage(response.body().getMessage());
                    builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            if (progressDialog != null) {
                                progressDialog.dismiss();
                            }

                            // Login Success => remember login credentials
                            //Member.getInstance(getApplicationContext()).rememberLoginCredentials(response.body().getAuth());

                            // Send local broadcast for state changing
                            Intent intent = new Intent(Member.MEMBER_STATE_CHANGE_KEY);
                            /*if (applyEcardCB.isChecked()) {
                                intent.putExtra("next", "ecard_apply");
                            }*/
                            LocalBroadcastManager.getInstance(RegisterActivity.this).sendBroadcast(intent);

                            // Set result for previous activity
                            setResult(RESULT_OK);
                            finish();
                        }
                    });
                    builder.show();
                } else if (response.errorBody() != null) {
                    try {
                        Toast.makeText(RegisterActivity.this, response.errorBody().string(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    progressDialog.dismiss();
                }

                call = null;
            }

            @Override
            public void onFailure(Call<AccountRegisterResponse> call, Throwable t) {
                Log.i("error", t.getLocalizedMessage());
                if (!call.isCanceled()) {
                    Toast.makeText(RegisterActivity.this, R.string.failed_to_connect, Toast.LENGTH_SHORT).show();
                    if (progressDialog != null) {
                        progressDialog.dismiss();
                    }

                    call = null;
                }
            }
        });
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (false) {
            // All requests other than Google & Facebook login always have higher priority
        } else {
            loginHandler.onActivityResult(requestCode, resultCode, data);
        }

        Log.i("activityResult", "request = " + requestCode + " | result = " + resultCode);
    }
}
