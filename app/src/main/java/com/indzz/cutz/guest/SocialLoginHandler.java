package com.indzz.cutz.guest;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.indzz.cutz.R;
//import com.indzz.cutz.api.DataSyncApi;
//import com.indzz.cutz.api.LoginResponse;
import com.indzz.cutz.api.DataSyncApi;
import com.indzz.cutz.api.LoginResponse;
import com.indzz.cutz.helper.Member;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;

import java.io.IOException;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by indzz on 1/11/2016.
 */

public class SocialLoginHandler {
    private AppCompatActivity mActivity;

    private ProgressDialog progressDialog;

    private String provider;
    private String accountId;
    private String accessToken;

    // Facebook SDK
    private CallbackManager callbackManager;
    private FacebookCallback<LoginResult> fbCallback;

    // Google Service
    private GoogleApiClient mGoogleApiClient;

    public final static int REQUEST_GOOGLE_SIGN_IN = 1001;

    public SocialLoginHandler(AppCompatActivity activity) {
        mActivity = activity;

        // Facebook callback manager
        setupFacebookLogin();

        // Google Sign-in
        setupGoogleSignIn();
    }

    public void setupFacebookLogin() {
        callbackManager = CallbackManager.Factory.create();

        fbCallback = new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.i("facebook", "onSuccess");
                Log.i("facebook", "user id = " + loginResult.getAccessToken().getUserId());
                Log.i("facebook", "token = " + loginResult.getAccessToken().getToken());

                Set<String> permissions = loginResult.getRecentlyGrantedPermissions();

                // Check if the user allow the app to retrieve his/her email address
                if (!permissions.contains("email")) {
                    // Can't proceed if email could not be accessed
                    AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
                    builder.setTitle(R.string.error);
                    builder.setMessage(R.string.fb_login_must_authorize_email);
                    builder.setPositiveButton(R.string.ok, null);
                    builder.show();
                    return;
                }

                progressDialog = ProgressDialog.show(mActivity, mActivity.getString(R.string.loading), "", true);
                provider = "facebook";
                accountId = loginResult.getAccessToken().getUserId();
                accessToken = loginResult.getAccessToken().getToken();
                sendLoginRequestToServer(null);
            }

            @Override
            public void onCancel() {
                Log.i("facebook", "onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                Log.i("facebook", "onError | Error = " + error.getMessage());
            }
        };

        // Login Button View
        LoginButton loginButton = (LoginButton) mActivity.findViewById(R.id.facebook_button);

        Log.d("facebook", "onError | Error = " + loginButton);

        loginButton.setReadPermissions("email");
        loginButton.registerCallback(callbackManager, fbCallback);
    }

    public void setupGoogleSignIn() {
        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(mActivity.getString(R.string.google_web_server_client_id))
                .requestEmail()
                .build();

        GoogleApiClient.OnConnectionFailedListener failedListener = new GoogleApiClient.OnConnectionFailedListener() {
            @Override
            public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
                Log.i("google", "onConnectionFailed | connectionResult = " + connectionResult.getErrorMessage());
            }
        };

        mGoogleApiClient = new GoogleApiClient.Builder(mActivity)
                .enableAutoManage(mActivity, failedListener)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        // Login Button View
        Button loginButton = (Button) mActivity.findViewById(R.id.google_button);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog = ProgressDialog.show(mActivity, mActivity.getString(R.string.loading), "", true);

                Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
                mActivity.startActivityForResult(signInIntent, REQUEST_GOOGLE_SIGN_IN);
            }
        });
    }

    public CallbackManager getCallbackManager() {
        return callbackManager;
    }

    public FacebookCallback<LoginResult> getFacebookCallback() {
        return fbCallback;
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d("google", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            provider = "google";
            accountId = acct.getId();
            accessToken = acct.getIdToken();
            sendLoginRequestToServer(null);
        }
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_GOOGLE_SIGN_IN) {
            // Google Sign In
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        } else {
            // Try Facebook callback manager
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    public void sendLoginRequestToServer(String password) {

        DataSyncApi api = DataSyncApi.Factory.create(mActivity.getApplicationContext());
        Call<LoginResponse> call = password != null ? api.socialLogin(provider, accountId, accessToken, password) : api.socialLogin(provider, accountId, accessToken);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }

                if (response.code() == 200) {
                    // Login Success => remember login credentials
                    Member.getInstance(mActivity.getApplicationContext()).rememberLoginCredentials(response.body());

                    // Send local broadcast for state changing
                    Intent intent = new Intent(Member.MEMBER_STATE_CHANGE_KEY);
                    LocalBroadcastManager.getInstance(mActivity).sendBroadcast(intent);

                    // Set result for previous activity
                    mActivity.setResult(Activity.RESULT_OK);
                    mActivity.finish();
                } else if (response.code() == 403) {
                    // Ask for password
                    try {
                        showAskPasswordDialog(response.errorBody().string());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    progressDialog.dismiss();
                } else {
                    // Other error
                    try {
                        Toast.makeText(mActivity, response.errorBody().string(), Toast.LENGTH_SHORT).show();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    progressDialog.dismiss();

                    if (provider.equals("facebook")) {
                        // Logout Facebook
                        LoginManager.getInstance().logOut();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                if (progressDialog != null) {
                    progressDialog.dismiss();
                }
                Toast.makeText(mActivity, R.string.failed_to_connect, Toast.LENGTH_SHORT).show();

                if (provider.equals("facebook")) {
                    // Logout Facebook
                    LoginManager.getInstance().logOut();
                }
            }
        });
    }




    public void showAskPasswordDialog(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle(message);

        final EditText edittext = new EditText(mActivity);
        edittext.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        edittext.setTransformationMethod(PasswordTransformationMethod.getInstance());
        edittext.setHint(R.string.enter_password);
        builder.setView(edittext);
        builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                progressDialog = ProgressDialog.show(mActivity, mActivity.getString(R.string.loading), "", true);

                String password = edittext.getText().toString();
                sendLoginRequestToServer(password);
            }
        });
        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        builder.show();
    }
}
