package com.indzz.cutz.api;

/**
 * Created by indzz on 27/10/2016.
 */

public class AccountRegisterResponse {
    private String message;
    private LoginResponse auth;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public LoginResponse getAuth() {
        return auth;
    }

    public void setAuth(LoginResponse auth) {
        this.auth = auth;
    }
}
