package com.indzz.cutz.api;

import android.content.Context;
import android.util.Log;

import com.indzz.cutz.BuildConfig;
import com.indzz.cutz.helper.Member;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by indzz on 11/8/16.
 */

public interface DataSyncApi {
    public static final String baseUrl = BuildConfig.FLAVOR.equals("local") ? "http://192.168.1.225:8010" : "http://192.168.1.225:8010";
    //public static final String baseUrl = "https://app.edr.hk/indzz/";

    /**
     * Basic Setup
     */
    Gson gson = new GsonBuilder()
            .setDateFormat("yyyy-MM-dd HH:mm:ss")
//            .setLenient()
            .create();

    public static final Retrofit retrofitRaw = new Retrofit.Builder()
            .baseUrl(baseUrl)
            .build();

    public static final Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(baseUrl)
            .addConverterFactory(GsonConverterFactory.create(gson))
            .build();


    // Login
    @FormUrlEncoded
    @POST("api/customer/login")
    Call<LoginResponse> login(@Field("username") String username, @Field("password") String password);


    // Forget
    @FormUrlEncoded
    @POST("api/customer/forgot-password")
    Call<LoginResponse> forgetPassword(@Field("username_or_email") String username_or_email);


    // Registration Form
    @FormUrlEncoded
    @POST("api/customer/register")
    Call<AccountRegisterResponse> register(@Field("email") String email,
                                @Field("username") String username,
                                @Field("password") String password,
                                @Field("name") String name,
                                @Field("country_code") String country_code,
                                @Field("phone") String phone,
                                @Field("salutation") String salutation
    );


    // Login via Google/Facebook account
    @FormUrlEncoded
    @POST("membership/social_login.php")
    Call<LoginResponse> socialLogin(@Field("provider") String provider,
                                    @Field("account_id") String accountId,
                                    @Field("access_token") String accessToken);

    @FormUrlEncoded
    @POST("membership/social_login.php")
    Call<LoginResponse> socialLogin(@Field("provider") String provider,
                                    @Field("account_id") String accountId,
                                    @Field("access_token") String accessToken,
                                    @Field("password") String password);


    public static class Factory {
            public static DataSyncApi create(Context context) {
            HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
            logging.setLevel(HttpLoggingInterceptor.Level.HEADERS);

            OkHttpClient.Builder builder = new OkHttpClient().newBuilder();
            builder.addInterceptor(logging);
            builder.addInterceptor(new MemberAuthInterceptor(context));
            OkHttpClient client = builder.build();
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

            return retrofit.create(DataSyncApi.class);
        }
    }


    public static class MemberAuthInterceptor implements Interceptor {
        private Context mContext;

        public MemberAuthInterceptor(Context context) {
            mContext = context;
        }

        @Override
        public Response intercept(Chain chain) throws IOException {
//            Member member = Member.getInstance(mContext);
            Request request = chain.request().newBuilder()
                    //.addHeader("Member-Account-Id", String.valueOf(member.getAccountId()))
                    //.addHeader("Member-Session-Token", member.getSessionToken())
                    .addHeader("Accept", "application/json")
                    .addHeader("App-Language", "en")
                    .addHeader("App-Platform", "android/1.0.0")
                    .build();

            Log.i("requestUrl", request.url().toString());
            //Log.i("header", request.headers().toString());
//            Log.i("data", request.body());

            return chain.proceed(request);
        }
    }
}
