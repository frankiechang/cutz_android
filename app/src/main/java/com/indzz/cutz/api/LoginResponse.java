package com.indzz.cutz.api;

/**
 * Created by indzz on 14/10/2016.
 */

public class LoginResponse {
    private int accountId;
    private String email;
    private String sessionToken;
    private String nameChi;
    private String nameEng;
    private String profilePictureUrl;
    private boolean hasEcard;

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public String getNameChi() {
        return nameChi;
    }

    public void setNameChi(String nameChi) {
        this.nameChi = nameChi;
    }

    public String getNameEng() {
        return nameEng;
    }

    public void setNameEng(String nameEng) {
        this.nameEng = nameEng;
    }

    public String getProfilePictureUrl() {
        return profilePictureUrl;
    }

    public void setProfilePictureUrl(String profilePictureUrl) {
        this.profilePictureUrl = profilePictureUrl;
    }

    public boolean isHasEcard() {
        return hasEcard;
    }

    public void setHasEcard(boolean hasEcard) {
        this.hasEcard = hasEcard;
    }
}
