package com.indzz.cutz;

import android.content.Intent;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;

import com.indzz.cutz.helper.PreferenceHelper;

public class LaunchActivity extends AppCompatActivity {

    private Button loginButton;
    private Button registerButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_instruction);
        setContentView(R.layout.activity_launch_screen);
        countToStartMainScreen();
    }



    public void countToStartMainScreen() {
        new CountDownTimer(2000, 1000) {
        //new CountDownTimer(100, 1000) {
            @Override
            public void onFinish() {
                // Show instruction pages
                if (!PreferenceHelper.getInstance(getBaseContext()).getBoolean("instruction_done", false)) {
                    startInstructionScreen();
                } else {
                    //startMainScreen();
                    startInstructionScreen();
                }
            }

            @Override
            public void onTick(long millisUntilFinished) {
                // Nothing to do...
            }

        }.start();
    }

    protected void startInstructionScreen() {
        Intent intent = new Intent(LaunchActivity.this, InstructionActivity.class);
        startActivity(intent);
        finish();
    }

    protected void startMainScreen() {
        //startActivity(ActivityIntentUtils.homeIntent(this));
        //finish();
    }



}
