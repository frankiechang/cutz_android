package com.indzz.cutz.helper;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;

import com.indzz.cutz.R;
import com.indzz.cutz.api.LoginResponse;
//import com.indzz.cutz.api.LoginResponse;

import java.util.List;

/**
 * Created by indzz on 28/9/2016.
 */

public class Member {
    public static final String MEMBER_STATE_CHANGE_KEY = "main.member_state_changed";

    private static Member instance;
    private Context mContext;

    private int accountId = 0;
    private String email = null;
    private String sessionToken = null;
    private String nameChi = null;
    private String nameEng = null;
    private String profilePictureUrl = null;
    private boolean hasEcard = false;

    private SharedPreferences pref;

    public static Member getInstance(Context context) {
        if (instance == null) {
            instance = new Member(context);
        }
        return instance;
    }

    public Member(Context context) {
        mContext = context;
        pref = context.getSharedPreferences("membership", Context.MODE_PRIVATE);

        syncFromPrefs();
    }

    protected void syncFromPrefs() {
        accountId = pref.getInt("account_id", 0);
        email = pref.getString("email", null);
        sessionToken = pref.getString("session_token", null);
        nameChi = pref.getString("name_chi", null);
        nameEng = pref.getString("name_eng", null);
        profilePictureUrl = pref.getString("profile_picture_url", null);
        hasEcard = pref.getBoolean("has_ecard", false);
    }


    public void rememberLoginCredentials(LoginResponse data) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt("account_id", data.getAccountId());
        editor.putString("email", data.getEmail());
        editor.putString("session_token", data.getSessionToken());
        editor.putBoolean("has_ecard", data.isHasEcard());
        editor.commit();
        syncFromPrefs();
    }


    @Deprecated
    public void rememberLoginCredentials(int accountId, String email, String sessionToken) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putInt("account_id", accountId);
        editor.putString("email", email);
        editor.putString("session_token", sessionToken);
        editor.commit();

        syncFromPrefs();
    }


    public void forgetLoginCredentials() {
        pref.edit().clear().commit();
        syncFromPrefs();
    }


    public boolean isMember() {
        return accountId != 0 && email != null && sessionToken != null;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSessionToken() {
        return sessionToken;
    }

    public void setSessionToken(String sessionToken) {
        this.sessionToken = sessionToken;
    }

    public String getNameChi() {
        return nameChi;
    }

    public void setNameChi(String nameChi) {
        this.nameChi = nameChi;
    }

    public String getNameEng() {
        return nameEng;
    }

    public void setNameEng(String nameEng) {
        this.nameEng = nameEng;
    }

    public String getName() {
        if (!TextUtils.isEmpty(nameChi)) return nameChi;
        else if (!TextUtils.isEmpty(nameEng)) return nameEng;
        return null;
    }

    public String getProfilePictureUrl() {
        return profilePictureUrl;
    }

    public void setProfilePictureUrl(String profilePictureUrl) {
        this.profilePictureUrl = profilePictureUrl;
    }

    public boolean isHasEcard() {
        return hasEcard;
    }

    public void setHasEcard(boolean hasEcard) {
        this.hasEcard = hasEcard;
    }

    public void updateEcardState(boolean newState) {
        this.hasEcard = newState;

        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean("has_ecard", newState);
        editor.apply();
    }

    public static AlertDialog.Builder createLoginPrompt(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        /*
        builder.setTitle(R.string.member_only_page);
        builder.setMessage(R.string.do_you_want_to_login);
        builder.setPositiveButton(R.string.login_now, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                context.startActivity(ActivityIntentUtils.loginIntent(context));
            }
        });
        builder.setNeutralButton(R.string.have_no_account, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                context.startActivity(ActivityIntentUtils.registerIntent(context));
            }
        });
        builder.setNegativeButton(R.string.later, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        */

        return builder;
    }


    /*
    public FamilyMember getProfile() {
        if (!isMember()) {
            return null;
        }

        if (profile == null) {
            refreshProfile();
        }

        return profile;
    }

    public List<FamilyMember> myFamilyMembers(boolean includeMe) {
        if (!isMember()) {
            return null;
        }
        return FamilyMember.myFamilyMembers(getAccountId(), includeMe);
    }*/

}
