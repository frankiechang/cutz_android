package com.indzz.cutz.helper;

/**
 * Created by indzz on 28/9/2016.
 */

public interface RecyclerViewRowListener<T> {
    public void onRowClicked(T object);
}
