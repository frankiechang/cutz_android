package com.indzz.cutz.helper;

/**
 * Created by indzz on 28/9/2016.
 */

public interface RecyclerViewRowLongClickListener<T> {
    public boolean onRowLongClicked(T object);
}
