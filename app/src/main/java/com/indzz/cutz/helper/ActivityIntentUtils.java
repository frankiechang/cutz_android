package com.indzz.cutz.helper;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.customtabs.CustomTabsClient;
import android.support.customtabs.CustomTabsIntent;
import android.support.customtabs.CustomTabsServiceConnection;
import android.support.v4.content.ContextCompat;
import android.util.Log;

import com.indzz.cutz.R;
import com.indzz.cutz.customer.setting.AboutUsFragment;
import com.indzz.cutz.customer.setting.ContactUsFragment;
import com.indzz.cutz.customer.setting.SettingsFragment;
import com.indzz.cutz.customer.setting.TermsAndConsFragment;
import com.indzz.cutz.guest.LoginActivity;
import com.indzz.cutz.guest.LoginProfessionalActivity;
import com.indzz.cutz.guest.RegisterActivity;
import com.indzz.cutz.customer.setting.CustomerActivities;
import com.indzz.cutz.customer.setting.NavDrawerActivity;
import com.indzz.cutz.professional.setting.ProActivities;

/**
 * ActivityIntentUtils.class
 * This class constructs various intents of different activities,
 * in order to maintain a consistent way to launch activity.
 * <p>
 * Created by indzz on 27/9/2016.
 */

public class ActivityIntentUtils {



    /*  ======================================== Professional =========================================== */


    public static Intent professionalHomeIntent(Context packageContext) {
        Intent intent = new Intent(packageContext, ProActivities.HomeActivity.class);
        intent.putExtra(NavDrawerActivity.EXTRA_ID, R.id.nav_home);
        //intent.putExtra(NavDrawerActivity.EXTRA_FRAGMENT_NAME, HomeFragment.class.getName());
        intent.putExtra(NavDrawerActivity.EXTRA_SHOW_LOGO, true);
        return intent;
    }


    public static Intent proSettingsIntent(Context packageContext) {
        Log.i("indezz-setting", "==setting intent==");
        Intent intent = new Intent(packageContext, ProActivities.SettingsActivity.class);
        intent.putExtra(NavDrawerActivity.EXTRA_ID, R.id.nav_setting);
        intent.putExtra(NavDrawerActivity.EXTRA_FRAGMENT_NAME, com.indzz.cutz.professional.setting.SettingsFragment.class.getName());
        intent.putExtra(NavDrawerActivity.EXTRA_TITLE, R.string.setting);
        return intent;
    }

    public static Intent proAboutUsIntent(Context packageContext) {
        Log.i("indezz-setting", "==setting intent==");
        Intent intent = new Intent(packageContext, ProActivities.SettingsActivity.class);
        intent.putExtra(NavDrawerActivity.EXTRA_ID, R.id.nav_setting);
        intent.putExtra(NavDrawerActivity.EXTRA_FRAGMENT_NAME, com.indzz.cutz.professional.setting.AboutUsFragment.class.getName());
        intent.putExtra(NavDrawerActivity.EXTRA_TITLE, R.string.about_cutz);
        return intent;
    }

    public static Intent proContactUsIntent(Context packageContext) {
        Log.i("indezz-setting", "==setting intent==");
        Intent intent = new Intent(packageContext, ProActivities.SettingsActivity.class);
        intent.putExtra(NavDrawerActivity.EXTRA_ID, R.id.nav_setting);
        intent.putExtra(NavDrawerActivity.EXTRA_FRAGMENT_NAME, com.indzz.cutz.professional.setting.ContactUsFragment.class.getName());
        intent.putExtra(NavDrawerActivity.EXTRA_TITLE, R.string.about_cutz);
        return intent;
    }

    public static Intent proTermsIntent(Context packageContext) {
        Log.i("indezz-setting", "==setting intent==");
        Intent intent = new Intent(packageContext, ProActivities.SettingsActivity.class);
        intent.putExtra(NavDrawerActivity.EXTRA_ID, R.id.nav_setting);
        intent.putExtra(NavDrawerActivity.EXTRA_FRAGMENT_NAME, com.indzz.cutz.professional.setting.TermsAndConsFragment.class.getName());
        intent.putExtra(NavDrawerActivity.EXTRA_TITLE, R.string.about_cutz);
        return intent;
    }

    /*  ======================================== Professional =========================================== */





    /*  ==================================== Customer =========================================== */

    public static Intent homeIntent(Context packageContext) {
        Intent intent = new Intent(packageContext, CustomerActivities.HomeActivity.class);
        intent.putExtra(NavDrawerActivity.EXTRA_ID, R.id.nav_home);
        //intent.putExtra(NavDrawerActivity.EXTRA_FRAGMENT_NAME, HomeFragment.class.getName());
        intent.putExtra(NavDrawerActivity.EXTRA_SHOW_LOGO, true);
        return intent;
    }

    public static Intent hairIntent(Context packageContext) {
        Intent intent = new Intent(packageContext, CustomerActivities.HairSalonListActivity.class);
        intent.putExtra(NavDrawerActivity.EXTRA_ID, R.id.nav_hair);
        //intent.putExtra(NavDrawerActivity.EXTRA_FRAGMENT_NAME, HomeFragment.class.getName());
        intent.putExtra(NavDrawerActivity.EXTRA_SHOW_LOGO, true);
        return intent;
    }


    public static Intent settingsIntent(Context packageContext) {
        Log.i("indezz-setting", "==setting intent==");
        Intent intent = new Intent(packageContext, CustomerActivities.SettingsActivity.class);
        intent.putExtra(NavDrawerActivity.EXTRA_ID, R.id.nav_setting);
        intent.putExtra(NavDrawerActivity.EXTRA_FRAGMENT_NAME, SettingsFragment.class.getName());
        intent.putExtra(NavDrawerActivity.EXTRA_TITLE, R.string.setting);
        return intent;
    }

    public static Intent aboutUsIntent(Context packageContext) {
        Log.i("indezz-setting", "==setting intent==");
        Intent intent = new Intent(packageContext, CustomerActivities.SettingsActivity.class);
        intent.putExtra(NavDrawerActivity.EXTRA_ID, R.id.nav_setting);
        intent.putExtra(NavDrawerActivity.EXTRA_FRAGMENT_NAME, AboutUsFragment.class.getName());
        intent.putExtra(NavDrawerActivity.EXTRA_TITLE, R.string.about_cutz);
        return intent;
    }

    public static Intent contactUsIntent(Context packageContext) {
        Log.i("indezz-setting", "==setting intent==");
        Intent intent = new Intent(packageContext, CustomerActivities.SettingsActivity.class);
        intent.putExtra(NavDrawerActivity.EXTRA_ID, R.id.nav_setting);
        intent.putExtra(NavDrawerActivity.EXTRA_FRAGMENT_NAME, ContactUsFragment.class.getName());
        intent.putExtra(NavDrawerActivity.EXTRA_TITLE, R.string.about_cutz);
        return intent;
    }

    public static Intent TermsIntent(Context packageContext) {
        Log.i("indezz-setting", "==setting intent==");
        Intent intent = new Intent(packageContext, CustomerActivities.SettingsActivity.class);
        intent.putExtra(NavDrawerActivity.EXTRA_ID, R.id.nav_setting);
        intent.putExtra(NavDrawerActivity.EXTRA_FRAGMENT_NAME, TermsAndConsFragment.class.getName());
        intent.putExtra(NavDrawerActivity.EXTRA_TITLE, R.string.about_cutz);
        return intent;
    }
    /*  ==================================== Customer =========================================== */



    public static Intent loginIntent(Context context) {
        return new Intent(context, LoginActivity.class);
    }

    public static Intent registerIntent(Context context) {
        return new Intent(context, RegisterActivity.class);
    }

    public static Intent loginAsProIntent(Context context) {
        return new Intent(context, LoginProfessionalActivity.class);
    }

    public static Intent openUriIntent(String url) {
        return openUriIntent(Uri.parse(url));
    }

    public static Intent openUriIntent(Uri uri) {
        return new Intent(Intent.ACTION_VIEW, uri);
    }

    public static void openUrlWithChromeTab(Context context, String url) {
        if (isChromeCustomTabsSupported(context)) {
            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
            builder.setToolbarColor(ContextCompat.getColor(context, R.color.colorPrimary));
            builder.setShowTitle(true);
            CustomTabsIntent customTabsIntent = builder.build();
            customTabsIntent.launchUrl(context, Uri.parse(url));
        } else {
            context.startActivity(ActivityIntentUtils.openUriIntent(url));
        }
    }

    /**
     * Check if Chrome CustomTabs are supported.
     * Some devices don't have Chrome or it may not be
     * updated to a version where custom tabs is supported.
     *
     * @param context the context
     * @return whether custom tabs are supported
     */
    public static boolean isChromeCustomTabsSupported(final Context context) {
        Intent serviceIntent = new Intent("android.support.customtabs.action.CustomTabsService");
        serviceIntent.setPackage("com.android.chrome");

        CustomTabsServiceConnection serviceConnection = new CustomTabsServiceConnection() {
            @Override
            public void onCustomTabsServiceConnected(final ComponentName componentName, final CustomTabsClient customTabsClient) {
            }

            @Override
            public void onServiceDisconnected(final ComponentName name) {
            }
        };

        boolean customTabsSupported = context.bindService(serviceIntent, serviceConnection, Context.BIND_AUTO_CREATE | Context.BIND_WAIVE_PRIORITY);

        return customTabsSupported && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;
    }


}
