package com.indzz.cutz.helper;

import android.annotation.TargetApi;
import android.app.TimePickerDialog;
import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.TimePicker;

import java.util.ArrayList;
import java.util.List;

public class TimePickerButton extends Button {

    List<OnTimeChangeListener> listeners = new ArrayList<OnTimeChangeListener>();

    private TimePickerDialog myTimePickerDialog;
    private int hour = 9;
    private int minute = 0;
    private boolean timeChosen = false;

    public TimePickerButton(Context context) {
        super(context);
        if (!isInEditMode()) {
            setupButton(context, null);
        }

    }

    public TimePickerButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            setupButton(context, attrs);
        }
    }

    public TimePickerButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode()) {
            setupButton(context, attrs);
        }
    }

    @TargetApi(21)
    public TimePickerButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        if (!isInEditMode()) {
            setupButton(context, attrs);
        }
    }

    public void setupButton(final Context context, AttributeSet attrs) {
        myTimePickerDialog = new TimePickerDialog(context, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int h, int m) {
                timeChosen = true;
                hour = h;
                minute = m;

                setText(formattedTime());

                for (int i = 0; i<listeners.size(); i++) {
                    listeners.get(i).onChange();
                }
            }
        }, hour, minute, false);

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                myTimePickerDialog.show();
            }
        });
    }

    public TimePickerDialog getTimePickerDialog() {
        return myTimePickerDialog;
    }

    public String formattedTime() {
        return String.format("%02d:%02d", hour, minute);
    }

    public int getHour() {
        return hour;
    }

    public int getMinute() {
        return minute;
    }

    public boolean isSelected() {
        return timeChosen;
    }

    public void addOnTimeChangeListener(OnTimeChangeListener listener) {
        listeners.add(listener);
    }

    public interface OnTimeChangeListener {
        public void onChange();
    }
}
