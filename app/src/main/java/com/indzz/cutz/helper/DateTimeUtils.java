package com.indzz.cutz.helper;

import android.content.Context;

import com.indzz.cutz.R;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by indzz on 28/9/2016.
 */

public class DateTimeUtils {
    public static String getCurrentServerTimeString() {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return formatter.format(new Date());
    }

    private static SimpleDateFormat dateSdf;
    public static SimpleDateFormat dateFormat() {
        if (dateSdf == null) {
            dateSdf = new SimpleDateFormat("yyyy-MM-dd");
        }
        return dateSdf;
    }

    private static SimpleDateFormat dateTimeSdf;
    public static SimpleDateFormat dateTimeFormat() {
        if (dateTimeSdf == null) {
            dateTimeSdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        }
        return dateTimeSdf;
    }

    private static SimpleDateFormat displayTimeSdf;
    public static SimpleDateFormat displayTimeFormat() {
        if (displayTimeSdf == null) {
            displayTimeSdf = new SimpleDateFormat("h:mm a");
        }
        return displayTimeSdf;
    }

    public static String weekdayShortNameToLocalizedName(Context context, String shortName) {
        if ("mon".equals(shortName)) {
            return context.getString(R.string.weekday_mon);
        } else if ("tue".equals(shortName)) {
            return context.getString(R.string.weekday_tue);
        } else if ("wed".equals(shortName)) {
            return context.getString(R.string.weekday_wed);
        } else if ("thr".equals(shortName)) {
            return context.getString(R.string.weekday_thu);
        } else if ("fri".equals(shortName)) {
            return context.getString(R.string.weekday_fri);
        } else if ("sat".equals(shortName)) {
            return context.getString(R.string.weekday_sat);
        } else if ("sun".equals(shortName)) {
            return context.getString(R.string.weekday_sun);
        } else if ("ph".equals(shortName)) {
            return context.getString(R.string.weekday_ph);
        }

        return null;
    }
}
