package com.indzz.cutz.helper;

import android.text.TextUtils;
import android.util.Patterns;

/**
 * Created by indzz on 12/10/2016.
 */

public class FormUtils {
    public static boolean isEmailValid(CharSequence target) {
        return !TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public static boolean isPasswordValid(String target) {
        String pattern = "[0-9]+[a-zA-Z_]+\\w*|[a-zA-Z_]+[0-9]+\\w*";
        return target.length() >= 8 && target.matches(pattern);
    }

    public static boolean isWeightOrHeightValid(String target) {
        String pattern = "^[0-9]{1,3}([\\.][0-9]{1,2})?$";
        return target.matches(pattern);
    }

    public static boolean isPhoneValid(String target) {
        return target.matches("^([23456789])([0-9]{7})$");
    }

    public static boolean isMobilePhoneValid(String target) {
        return target.matches("^([456789])([0-9]{7})$");
    }

}
