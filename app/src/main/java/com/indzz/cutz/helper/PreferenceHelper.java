package com.indzz.cutz.helper;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by indzz on 6/8/16.
 */
public class PreferenceHelper {
    private SharedPreferences pref;
    private static Context mCtx;

    private static PreferenceHelper mInstance;

    public static synchronized PreferenceHelper getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new PreferenceHelper(context);
        }
        return mInstance;
    }

    public PreferenceHelper(Context context) {
        mCtx = context;
        pref = context.getSharedPreferences("config", Context.MODE_PRIVATE);
    }

    public void update(String key, boolean value) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public void update(String key, String value) {
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public boolean getBoolean(String key, boolean defaultOption) {
        return pref.getBoolean(key, defaultOption);
    }

    public String getString(String key, String defaultOption) {
        return pref.getString(key, defaultOption);
    }

    public void remove(String key) {
        SharedPreferences.Editor editor = pref.edit();
        editor.remove(key);
        editor.apply();
    }
}
