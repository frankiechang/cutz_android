package com.indzz.cutz.helper;

import android.os.Bundle;

import com.google.firebase.analytics.FirebaseAnalytics;

/**
 * Created by indzz on 17/2/2017.
 */

public class AnalyticsHelper {
    private static FirebaseAnalytics mFba;

    public static void setup(FirebaseAnalytics fba) {
        mFba = fba;
    }

    public static void logEvent(FirebaseAnalytics fba, String event, String itemId, String itemName) {
        if (mFba == null) {
            mFba = fba;
        }

        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, itemId);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, itemName);
        fba.logEvent(event, bundle);
    }

    public static void logChooseSpeciality(FirebaseAnalytics fba, String specId, String specName, String referrer) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, specId);
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, specName);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Speciality");
        bundle.putString(FirebaseAnalytics.Param.SOURCE, referrer);
        fba.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    public static void logChooseDistrict(FirebaseAnalytics fba, int districtId, String districtName, String referrer) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, String.valueOf(districtId));
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, districtName);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "District");
        bundle.putString(FirebaseAnalytics.Param.SOURCE, referrer);
        fba.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    public static void logViewProfile(FirebaseAnalytics fba, int doctorId, String drName) {
        Bundle bundle = new Bundle();
        bundle.putString(FirebaseAnalytics.Param.ITEM_ID, String.valueOf(doctorId));
        bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, drName);
        bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "Doctor");
        fba.logEvent(FirebaseAnalytics.Event.SELECT_CONTENT, bundle);
    }

    public static FirebaseAnalytics getFba() {
        return mFba;
    }
}
