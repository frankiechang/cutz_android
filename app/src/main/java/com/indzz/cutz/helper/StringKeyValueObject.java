package com.indzz.cutz.helper;

/**
 * Created by indzz on 8/10/2016.
 */

public class StringKeyValueObject {
    protected String key;
    protected Object value;

    public StringKeyValueObject(String key, Object value) {
        this.key = key;
        this.value = value;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String toString() {
        return this.value.toString();
    }
}
