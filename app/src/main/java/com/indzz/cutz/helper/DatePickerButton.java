package com.indzz.cutz.helper;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;

import com.indzz.cutz.R;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DatePickerButton extends Button {

    List<OnDateChangeListener> listeners = new ArrayList<OnDateChangeListener>();

    private DatePickerDialog myDatePicker;
    private Date selectedDate;

    public static final int TYPE_NORMAL = 0;
    public static final int TYPE_DOB = 1;

    public DatePickerButton(Context context) {
        super(context);
        if (!isInEditMode()) {
            setupButton(context, null);
        }

    }

    public DatePickerButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (!isInEditMode()) {
            setupButton(context, attrs);
        }
    }

    public DatePickerButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        if (!isInEditMode()) {
            setupButton(context, attrs);
        }
    }

    public DatePickerButton(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        if (!isInEditMode()) {
            setupButton(context, attrs);
        }
    }

    public void setupButton(final Context context, AttributeSet attrs) {
        final Calendar c = Calendar.getInstance();
        c.add(Calendar.DAY_OF_YEAR, 7);
        int startYear = c.get(Calendar.YEAR);
        int startMonth = c.get(Calendar.MONTH);
        int startDay = c.get(Calendar.DAY_OF_MONTH);

        TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.DatePickerButton, 0, 0);
        int customType = 0;
        String customMinDate;
        String customMaxDate;
        try {
            customType = a.getInteger(R.styleable.DatePickerButton_type, 0);
            customMinDate = a.getString(R.styleable.DatePickerButton_minDate);
            customMaxDate = a.getString(R.styleable.DatePickerButton_maxDate);
        } finally {
            a.recycle();
        }

        Log.i("INDZZ", "customType = " + customType);

        if (customType == TYPE_DOB) {
            // Date of birth - Default date
            startYear = 1980;
            startMonth = Calendar.JANUARY;
            startDay = 1;
        }

        /*if (minDate <= maxDate) {
            myDatePicker.getDatePicker().setMinDate(new Date().getTime()+minDate*24*3600*1000);
            myDatePicker.getDatePicker().setMaxDate(new Date().getTime()+maxDate*24*3600*1000);
        }*/
        myDatePicker = new DatePickerDialog(context, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth, 0, 0, 0);

                if (newDate.getTimeInMillis() < datePicker.getMinDate()) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(context);
                    alert.setTitle(R.string.error);
                    alert.setMessage(R.string.invalid_date);
                    alert.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });
                    alert.show();
                    return;
                }

                selectedDate = newDate.getTime();
                setText(formattedServiceDate());

                for (int i=0; i<listeners.size(); i++) {
                    listeners.get(i).onChange();
                }
            }
        }, startYear, startMonth, startDay);

        if (customType == TYPE_DOB) {
            // Min Date = 1900/01/01
            Calendar minCalendar = Calendar.getInstance();
            minCalendar.set(1900, Calendar.JANUARY, 1);
            myDatePicker.getDatePicker().setMinDate(minCalendar.getTimeInMillis());

            // Max Date = 18 years ago
            Calendar maxCalendar = Calendar.getInstance();
            maxCalendar.add(Calendar.YEAR, -18);
            myDatePicker.getDatePicker().setMaxDate(maxCalendar.getTimeInMillis());
        }

        myDatePicker.setTitle("");

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                myDatePicker.show();
            }
        });
    }

    public DatePickerDialog getDatePickerDialog() {
        return myDatePicker;
    }

    public DatePicker getDatePicker() {
        return myDatePicker.getDatePicker();
    }

    public String formattedServiceDate() {
        return getFormattedDate();
    }

    public String getFormattedDate() {
        if (selectedDate == null) {
            return "";
        }
        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormatter.format(selectedDate);
    }

    public Date getSelectedDate() {
        return selectedDate;
    }

    public boolean isSelected() {
        return selectedDate != null;
    }

    public void addOnDateChangeListener(OnDateChangeListener listener) {
        listeners.add(listener);
    }

    public void selectDate(Date date) {
        selectedDate = date;

        Calendar c = Calendar.getInstance();
        c.setTime(selectedDate);

        getDatePicker().updateDate(c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));

        setText(formattedServiceDate());
        if (myDatePicker != null) {
            myDatePicker.setTitle("");
        }
    }

    public void setMinDate(Date date) {
        getDatePicker().setMinDate(0);
        getDatePicker().setMinDate(date.getTime());
    }

    public void setMaxDate(Date date) {
        getDatePicker().setMaxDate(0);
        getDatePicker().setMaxDate(date.getTime());
    }

    public interface OnDateChangeListener {
        public void onChange();
    }
}
