package com.indzz.cutz.helper;

import android.Manifest;
import android.app.Activity;
import android.content.ClipData;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;

import com.indzz.cutz.R;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by indzz on 22/10/2016.
 */

public class SelectPhotoHelper {
    public final static int REQUEST_ADD_PHOTO_BY_CAMERA = 801;
    public final static int REQUEST_ADD_PHOTO_FROM_GALLERY = 802;

    public final static int PERMISSIONS_REQUEST_CAMERA = 803;

    private Context context;
    private String mCurrentPhotoPath;
    private Uri cameraPhotoUri;
    private boolean allowMultiple = false;

    public SelectPhotoHelper(Context context, boolean allowMultiple) {
        this.context = context;
        this.allowMultiple = allowMultiple;
    }

    public void pickOrCapturePhoto() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(R.string.choose_an_action);

        if (context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA)) {
            CharSequence[] items = {context.getString(R.string.action_capture_photo), context.getString(R.string.action_choose_photo)};
            builder.setItems(items, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (which == 1) {
                        // Gallery
                        pickPhoto();
                    } else if (which == 0) {
                        // Camera
                        checkPermissionAndStartCamera();
                    }
                }
            });
            builder.show();
        } else {
            // Gallery
            pickPhoto();
        }
    }

    public void pickPhoto() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, allowMultiple);
        }
        ((Activity) context).startActivityForResult(intent, REQUEST_ADD_PHOTO_FROM_GALLERY);
    }

    public void checkPermissionAndStartCamera() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
            ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            Log.i("camera", "permission is not granted, ask the user now");
            ActivityCompat.requestPermissions((Activity)context, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, PERMISSIONS_REQUEST_CAMERA);
        } else {
            Log.i("camera", "permission has already been granted");
            capturePhoto();
        }
    }

    public void capturePhoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(context.getPackageManager()) != null) {
            // Create the File where the photo should go
            /*File photoFile = null;
            try {
                photoFile = FileUtils.createImageFile(context);
                mCurrentPhotoPath = photoFile.getAbsolutePath();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Toast.makeText(context, context.getString(R.string.failed_to_create_image_file), Toast.LENGTH_SHORT).show();
                return;
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                //Uri photoURI = Uri.fromFile(photoFile);
                //Uri photoURI = FileProvider.getUriForFile(context, BuildConfig.APPLICATION_ID + ".provider", photoFile);
                Uri photoURI  = context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new ContentValues());

                Log.i("camera", photoFile.getAbsolutePath());
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                ((Activity)context).startActivityForResult(takePictureIntent, SelectPhotoHelper.REQUEST_ADD_PHOTO_BY_CAMERA);
            }*/

            cameraPhotoUri = context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new ContentValues());

            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, cameraPhotoUri);
            ((Activity)context).startActivityForResult(takePictureIntent, SelectPhotoHelper.REQUEST_ADD_PHOTO_BY_CAMERA);
        }
    }

    public String getCurrentPhotoPath() {
        return mCurrentPhotoPath;
    }

    public List<Uri> handleResultFromCamera(Intent data) {
        /*File f = new File(getCurrentPhotoPath());
        Uri contentUri = Uri.fromFile(f);*/

        List<Uri> list = new ArrayList<>();
        list.add(cameraPhotoUri);
        return list;
    }

    public List<Uri> handleResultFromGallery(Intent data) {
        List<Uri> list = new ArrayList<>();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2 && data.getClipData() != null) {
            // Jelly Bean MR2 or above support multiple photos
            ClipData clipdata = data.getClipData();
            for (int i = 0; i < clipdata.getItemCount(); i++) {
                Uri imageUri = clipdata.getItemAt(i).getUri();
                Log.i("indzz", "imageUri = " + imageUri);
                if (imageUri != null) {
                    try {
                        File newImage = FileUtils.createImageFile(context);
                        FileUtils.copy(context.getContentResolver().openInputStream(imageUri), new FileOutputStream(newImage));
                        list.add(Uri.parse(newImage.getAbsolutePath()));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {
            // Single photo selected only
            Uri imageUri = null;
            if(data.getData() != null){
                imageUri = data.getData();
            } else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                ClipData clipData = data.getClipData();
                ClipData.Item item = clipData.getItemAt(0);
                imageUri = item.getUri();
            }

            if (imageUri != null) {
                try {
                    File newImage = FileUtils.createImageFile(context);
                    FileUtils.copy(context.getContentResolver().openInputStream(imageUri), new FileOutputStream(newImage));
                    list.add(Uri.parse(newImage.getAbsolutePath()));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return list;
    }
}
