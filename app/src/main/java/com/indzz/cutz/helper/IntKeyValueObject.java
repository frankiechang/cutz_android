package com.indzz.cutz.helper;

/**
 * Created by indzz on 8/10/2016.
 */

public class IntKeyValueObject {
    protected int key;
    protected Object value;

    public IntKeyValueObject(int key, Object value) {
        this.key = key;
        this.value = value;
    }

    public int getKey() {
        return key;
    }

    public void setKey(int key) {
        this.key = key;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String toString() {
        return this.value.toString();
    }
}
