package com.indzz.cutz.helper;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Created by indzz on 28/9/2016.
 */

public class NetworkUtils {
    public static boolean isConnected(Context context) {
        ConnectivityManager conMgr = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = conMgr.getActiveNetworkInfo();

        Log.i("activeNetwork", activeNetwork != null ? "not null" : "null");
        Log.i("activeNetwork", activeNetwork.isConnected() ? "connected" : "not connected");

        return activeNetwork != null && activeNetwork.isConnected();
    }
}
