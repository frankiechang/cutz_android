package com.edr.entities;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by indzz on 12/10/2016.
 */

public class MapLocation implements Parcelable {
    private int doctorId;
    private String name;
    private String description;
    private String address;
    private String phone;
    private double lat;
    private double lng;

    public MapLocation() {

    }

    public MapLocation(DoctorProfile profile) {
        doctorId = profile.getId().intValue();
        name = profile.getNameWithTitle();
        description = profile.getDescription();
        address = profile.getAddress();
        phone = profile.getTelephone();
        lat = profile.getLat();
        lng = profile.getLng();
    }

    protected MapLocation(Parcel in) {
        doctorId = in.readInt();
        name = in.readString();
        description = in.readString();
        address = in.readString();
        phone = in.readString();
        lat = in.readDouble();
        lng = in.readDouble();
    }

    public static final Creator<MapLocation> CREATOR = new Creator<MapLocation>() {
        @Override
        public MapLocation createFromParcel(Parcel in) {
            return new MapLocation(in);
        }

        @Override
        public MapLocation[] newArray(int size) {
            return new MapLocation[size];
        }
    };

    public int getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(int doctorId) {
        this.doctorId = doctorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(doctorId);
        dest.writeString(name);
        dest.writeString(description);
        dest.writeString(address);
        dest.writeString(phone);
        dest.writeDouble(lat);
        dest.writeDouble(lng);
    }
}
