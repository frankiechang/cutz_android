package com.indzz.cutz.entities;

import android.content.Context;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.BulletSpan;
import android.text.style.RelativeSizeSpan;

import com.indzz.cutz.helper.DateTimeUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by indzz on 30/9/2016.
 */

public class SalonProfile {

    private Long id = null;
    private String nameChi;
    private String nameEng;
    private String titleChi;
    private int gender;
    private int districtId;
    private String telephone;
    private String urgentPhone;
    private String fax;
    private String email;
    private String addressChi;
    private String addressEng;
    private List<String> qualification;
    private Map<String, String> consultationHours;
    private List<String> otherInfo;
    private String language;
    private boolean urgentService;
    private boolean night;
    private boolean ppi;
    private boolean ehealth;
    private boolean ecard;
    private boolean allowBooking;
    private boolean voucher;
    private String affiliatedChi;
    private String photoLink;
    private String description;
    private String url;
    private int likeCount1 = 0;
    private int likeCount2 = 0;
    private int likeCount3 = 0;
    private boolean isLiked1 = false;
    private boolean isLiked2 = false;
    private boolean isLiked3 = false;
    private double lat;
    private double lng;
    private double distance = -1.0;
    private boolean openWeekday = false;
    private boolean openWeekend = false;
    private boolean openPublicHoliday = false;

    public final static int GENDER_MALE = 1;
    public final static int GENDER_FEMALE = 2;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNameChi() {
        return nameChi;
    }

    public void setNameChi(String nameChi) {
        this.nameChi = nameChi;
    }

    public String getNameEng() {
        return nameEng;
    }

    public void setNameEng(String nameEng) {
        this.nameEng = nameEng;
    }

    public String getTitleChi() {
        return titleChi;
    }

    public void setTitleChi(String titleChi) {
        this.titleChi = titleChi;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public int getDistrictId() {
        return districtId;
    }

    public void setDistrictId(int districtId) {
        this.districtId = districtId;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public boolean hasValidTelephone() {
        return !TextUtils.isEmpty(telephone) && telephone.matches("^[0-9]{8}$");
    }

    public String getUrgentPhone() {
        return urgentPhone;
    }

    public void setUrgentPhone(String urgentPhone) {
        this.urgentPhone = urgentPhone;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAddressChi() {
        return addressChi;
    }

    public void setAddressChi(String addressChi) {
        this.addressChi = addressChi;
    }

    public String getAddress() {
        if (!TextUtils.isEmpty(addressChi)) return addressChi;
        if (!TextUtils.isEmpty(addressEng)) return addressEng;
        return null;
    }

    public boolean hasAddress() {
        return !TextUtils.isEmpty(addressChi) || !TextUtils.isEmpty(addressEng);
    }

    public String getNameWithTitle() {
        return String.format("%s%s", nameChi, titleChi);
    }

    public List<String> getQualification() {
        return qualification;
    }

    public void setQualification(List<String> qualification) {
        this.qualification = qualification;
    }

    public String getQualificationJoined(String glue) {
        List<String>qualifications = new ArrayList<>();
        for (String q : getQualification()) {
            qualifications.add("- "+q);
        }
        return TextUtils.join(glue, qualifications);
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public boolean isUrgentService() {
        return urgentService;
    }

    public void setUrgentService(boolean urgentService) {
        this.urgentService = urgentService;
    }

    public boolean isNight() {
        return night;
    }

    public void setNight(boolean night) {
        this.night = night;
    }

    public boolean isPpi() {
        return ppi;
    }

    public void setPpi(boolean ppi) {
        this.ppi = ppi;
    }

    public boolean isEhealth() {
        return ehealth;
    }

    public void setEhealth(boolean ehealth) {
        this.ehealth = ehealth;
    }

    public boolean isEcard() {
        return ecard;
    }

    public void setEcard(boolean ecard) {
        this.ecard = ecard;
    }

    public String getAffiliatedChi() {
        return affiliatedChi;
    }

    public void setAffiliatedChi(String affiliatedChi) {
        this.affiliatedChi = affiliatedChi;
    }

    public String getPhotoLink() {
        return photoLink;
    }

    public void setPhotoLink(String photoLink) {
        this.photoLink = photoLink;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getLikeCount1() {
        return likeCount1;
    }

    public void setLikeCount1(int likeCount1) {
        this.likeCount1 = likeCount1;
    }

    public int getLikeCount2() {
        return likeCount2;
    }

    public void setLikeCount2(int likeCount2) {
        this.likeCount2 = likeCount2;
    }

    public int getLikeCount3() {
        return likeCount3;
    }

    public void setLikeCount3(int likeCount3) {
        this.likeCount3 = likeCount3;
    }

    public boolean isLiked1() {
        return isLiked1;
    }

    public void setLiked1(boolean liked1) {
        isLiked1 = liked1;
    }

    public boolean isLiked2() {
        return isLiked2;
    }

    public void setLiked2(boolean liked2) {
        isLiked2 = liked2;
    }

    public boolean isLiked3() {
        return isLiked3;
    }

    public void setLiked3(boolean liked3) {
        isLiked3 = liked3;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getAddressEng() {
        return addressEng;
    }

    public void setAddressEng(String addressEng) {
        this.addressEng = addressEng;
    }

    public Map<String, String> getConsultationHours() {
        return consultationHours;
    }

    public String getConsultationHoursJoined(Context context, String glue) {
        List<String>items = new ArrayList<>();
        for (String key : getConsultationHours().keySet()) {
            String value = getConsultationHours().get(key);

            if (!key.equals("note")) {
                //items.add(String.format("%s：%s", DateTimeUtils.weekdayShortNameToLocalizedName(context, key), value));
            } else {
                items.add(value);
            }

        }
        return TextUtils.join(glue, items);
    }

    public Spannable getQualificationSpannable() {
        SpannableStringBuilder sb = new SpannableStringBuilder();

        List<String> qualifications = getQualification();

        for (int i = 0; i < qualifications.size(); i++) {
            boolean notLastLine = i < qualifications.size()-1;

            CharSequence line = qualifications.get(i) + (notLastLine ? "\n" : "");
            Spannable spannable = new SpannableString(line);
            spannable.setSpan(new BulletSpan(15), 0, spannable.length(), Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
            sb.append(spannable);

            if (notLastLine) {
                Spannable lineMargin = new SpannableString("\n");
                lineMargin.setSpan(new RelativeSizeSpan(0.5f), 0, 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                sb.append(lineMargin);
            }
        }
        return sb;
    }

    public void setConsultationHours(Map<String, String> consultationHours) {
        this.consultationHours = consultationHours;
    }

    public boolean isAllowBooking() {
        return allowBooking;
    }

    public void setAllowBooking(boolean allowBooking) {
        this.allowBooking = allowBooking;
    }

    public List<String> getOtherInfo() {
        return otherInfo;
    }

    public void setOtherInfo(List<String> otherInfo) {
        this.otherInfo = otherInfo;
    }

    public String getOtherInfoJoined(String glue) {
        return TextUtils.join(glue, getOtherInfo());
    }

    public Spannable getOtherInfoSpannable() {
        SpannableStringBuilder sb = new SpannableStringBuilder();

        List<String> lines = getOtherInfo();

        for (int i = 0; i < lines.size(); i++) {
            boolean notLastLine = i < lines.size()-1;

            CharSequence line = lines.get(i) + (notLastLine ? "\n" : "");
            sb.append(new SpannableString(line));

            if (notLastLine) {
                Spannable lineMargin = new SpannableString("\n");
                lineMargin.setSpan(new RelativeSizeSpan(0.5f), 0, 1, Spannable.SPAN_INCLUSIVE_EXCLUSIVE);
                sb.append(lineMargin);
            }
        }
        return sb;
    }

    public boolean isVoucher() {
        return voucher;
    }

    public void setVoucher(boolean voucher) {
        this.voucher = voucher;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public boolean isOpenWeekday() {
        return openWeekday;
    }

    public void setOpenWeekday(boolean openWeekday) {
        this.openWeekday = openWeekday;
    }

    public boolean isOpenWeekend() {
        return openWeekend;
    }

    public void setOpenWeekend(boolean openWeekend) {
        this.openWeekend = openWeekend;
    }

    public boolean isOpenPublicHoliday() {
        return openPublicHoliday;
    }

    public void setOpenPublicHoliday(boolean openPublicHoliday) {
        this.openPublicHoliday = openPublicHoliday;
    }


}
